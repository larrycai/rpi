# Introduction

This is a simple home geek project to show live data on LCD at home wrapped with LEGO bricks

![](traefik/pistation.png)

# Hardware Raspberry Pi zero wh + LCD1602

* [Raspberry Pi Zero](https://www.raspberrypi.com/products/raspberry-pi-zero/) WH
* [I2CLCD1602](https://docs.sunfounder.com/projects/umsk/en/latest/01_components_basic/26-component_i2c_lcd1602.html#cpn-i2c-lcd1602), I bought it from [Freenove Super Starter Kit for Raspberry Pi package](https://store.freenove.com/products/fnk0019), it has code under https://github.com/Freenove/Freenove_Super_Starter_Kit_for_Raspberry_Pi 
* microSD 32G
* cable (microHDMI -> HDMI, power Cable with switch, microUSB -> USB)

![](raspberrypizero.png)

## Preparation for Raspberry Pi OS

* Burn microSD for latest raspberrypi os https://www.raspberrypi.com/software/
* connect cable (can switch USB mouse/keyboard since it has one only)
* configure wifi (TV monitor)
* enable ssh (`sudo raspi-config`)
* reboot, if it works turn it into boot into cli (ip is fixed mostly `192.168.0.25`/`pi` & `raspberry` by default)

## circuit

![](https://docs.sunfounder.com/projects/umsk/en/latest/_images/Lesson_26_LCD1602_Pi_bb.png)

# Software

below can be used for debug, or run small individual app

```
$ ./livelcd3.py debug # can be local or connected to pi
```

## Live data

The data from different source may need subscription, select fit for your needs

* trafik data for live timetable: see [traefik](traefik), it has token limitation issues, so 2-3 minutes once, good enough
* weather: Use [SMHI Opendata](https://opendata.smhi.se/), originally it is [pyowm](https://github.com/csparpa/pyowm) module using data from https://openweathermap.org/api, and it needs subscription now
* moon phase & sunrise/sunset: [astral](https://astral.readthedocs.io) module

## development

* some freenode script `Adafruit_LCD1602.py`, `PCF8574.py`
* use `smbus` (not easy to install on mac), [smbus2](https://github.com/kplindegaard/smbus2) is tricky to have on raspberrypi, so keep `smbus` only for now

# reference

## other tried circuit
* power module https://microcontrollerslab.com/mb102-breadboard-power-supply-module-pinout-and-how-to-use-it/
* electrial https://theengineeringmindset.com/electrical/
  * led: https://theengineeringmindset.com/led-circuit-design-how-to-design-led-circuits/

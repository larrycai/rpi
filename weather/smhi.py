#!/usr/bin/env python3
# coding: utf-8

import requests
import datetime
import json


# Function to get weather data from SMHI API
# https://opendata.smhi.se/apidocs/metfcst/get-forecast.html
def get_weather_data(lat, lon):
    url = f"https://opendata-download-metfcst.smhi.se/api/category/pmp3g/version/2/geotype/point/lon/{lon}/lat/{lat}/data.json"
    response = requests.get(url)
    data = response.json()
    return data


def get_weather_type(symbol):
    symbol_table = [
        "Sunny",
        "Sunny",
        "Cloud",
        "Cloud",
        "Cloud",
        "Cloud",
        "Fog",
        "Rain",
        "Rain",
        "Rain",
        "Tstrm",
        "Sleet",
        "Sleet",
        "Sleet",
        "Snow",
        "Snow",
        "Snow",
        "Rain",
        "Rain",
        "Rain",
        "Thndr",
        "Sleet",
        "Sleet",
        "Sleet",
        "Snow",
        "Snow",
        "Snow",
    ]
    return symbol_table[symbol - 1]


# Function to extract current weather, today's weather, and tomorrow's high/low temperatures
def extract_weather_info(data):
    current_temp = None
    current_wind = None
    today_high = None
    today_low = None
    tomorrow_high = None
    tomorrow_low = None
    today = datetime.datetime.now().date()
    tomorrow = today + datetime.timedelta(days=1)

    # https://opendata.smhi.se/apidocs/metfcst/parameters.html

    for time_series in data["timeSeries"]:
        valid_time = datetime.datetime.fromisoformat(time_series["validTime"][:-1])
        date = valid_time.date()
        # print(json.dumps(time_series["parameters"], indent=4))
        if date == today:
            temp = next(
                param["values"][0]
                for param in time_series["parameters"]
                if param["name"] == "t"
            )
            wind = next(
                param["values"][0]
                for param in time_series["parameters"]
                if param["name"] == "ws"
            )
            weather = next(
                param["values"][0]
                for param in time_series["parameters"]
                if param["name"] == "Wsymb2"
            )
            if current_temp is None:
                current_weather = weather
                current_temp = temp
                current_wind = wind
            if today_high is None or temp > today_high:
                today_high = temp
                today_weather = weather
            if today_low is None or temp < today_low:
                today_low = temp

        if date == tomorrow:
            temp = next(
                param["values"][0]
                for param in time_series["parameters"]
                if param["name"] == "t"
            )
            if tomorrow_high is None or temp > tomorrow_high:
                tomorrow_high = temp
                tomorrow_weather = weather
            if tomorrow_low is None or temp < tomorrow_low:
                tomorrow_low = temp

    return (
        [current_weather, current_temp],
        current_wind,
        [today_weather, today_high, today_low],
        [tomorrow_weather, tomorrow_high, tomorrow_low],
    )


# Function to print weather information
def print_weather_info(
    current,
    wind,
    today,
    tomorrow,
):
    print("Curr Temp %2d°C" % round(current[1]))
    print("%-5s Wind %2dm/s" % (get_weather_type(current[0]), round(wind)))
    print("Today Mix: %2d°C" % round(today[2]))
    print("%-5s Max: %2d°C" % (get_weather_type(today[0]), round(today[1])))
    print("TOMM Mix: %2d°C" % round(tomorrow[2]))
    print("%-5s Max: %2d°C" % (get_weather_type(tomorrow[0]), round(tomorrow[1])))


# Replace with your latitude and longitude
latitude = 59.45  # New position
longitude = 17.88  # New position

# Get weather data
weather_data = get_weather_data(latitude, longitude)

# Extract and print weather information
(
    current,
    wind,
    today,
    tomorrow,
) = extract_weather_info(weather_data)


print_weather_info(
    current,
    wind,
    today,
    tomorrow,
)

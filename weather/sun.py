#!/usr/bin/env python3
# coding: utf-8

from astral import LocationInfo
from astral.sun import sun
from datetime import date
import zoneinfo

ZONEINFO = "Europe/Stockholm"


def get_precise_sunrise_sunset(latitude, longitude):
    city = LocationInfo("City", "County", ZONEINFO, latitude, longitude)
    s = sun(city.observer, date=date.today(), tzinfo=zoneinfo.ZoneInfo(ZONEINFO))
    return s["sunrise"], s["sunset"]


# Example usage
latitude = 59.4281
longitude = 17.9506
sunrise, sunset = get_precise_sunrise_sunset(latitude, longitude)

print(f"Calculated Sunrise:{sunrise}, {sunrise.strftime('%H:%M')}")
print(f"Calculated Sunset: {sunset.strftime('%H:%M')}")

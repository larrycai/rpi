#!/usr/bin/env python3
# coding: utf-8
import requests
import urllib3
import sys
import os
from dotenv import load_dotenv

from time import sleep, strftime
from datetime import datetime, timedelta, date

from astral import moon

from astral import LocationInfo
from astral.sun import sun
import zoneinfo

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

DEBUG = False
lcd = mcp = None

# updated in 2024.10.5
# https://www.trafiklab.se/api/trafiklab-apis/resrobot-v21/
load_dotenv()

TRAFIKLAB_KEY = os.getenv("TRAFIKLAB_KEY")
TRAFIKLAB_SITEID = os.getenv("TRAFIKLAB_SITEID")
WEATHER_KEY = os.getenv("WEATHER_KEY")
LONGTITUDE = float(os.getenv("LONGTITUDE"))
LANTITUDE = float(os.getenv("LANTITUDE"))
ZONEINFO = "Europe/Stockholm"


RESROBOT_TIMETABLE_LEAVE_URL = "https://api.resrobot.se/v2.1/departureBoard"
DEFAULT_LATEST = {"arrive": "00:00:00", "leave": "00:00:00", "destination": "N"}


# Function to get weather data from SMHI API
# https://opendata.smhi.se/apidocs/metfcst/get-forecast.html
def get_weather_data():
    url = f"https://opendata-download-metfcst.smhi.se/api/category/pmp3g/version/2/geotype/point/lon/{LONGTITUDE}/lat/{LANTITUDE}/data.json"
    response = requests.get(url)
    data = response.json()
    return data


def get_weather_type(symbol):
    symbol_table = [
        "Sunny",
        "Sunny",
        "Cloud",
        "Cloud",
        "Cloud",
        "Cloud",
        "Fog",
        "Rain",
        "Rain",
        "Rain",
        "Tstrm",
        "Sleet",
        "Sleet",
        "Sleet",
        "Snow",
        "Snow",
        "Snow",
        "Rain",
        "Rain",
        "Rain",
        "Thndr",
        "Sleet",
        "Sleet",
        "Sleet",
        "Snow",
        "Snow",
        "Snow",
    ]
    return symbol_table[symbol - 1]


# Function to extract current weather, today's weather, and tomorrow's high/low temperatures
def get_weather():
    current_temp = None
    current_wind = None
    today_high = None
    today_low = None
    tomorrow_high = None
    tomorrow_low = None
    today = datetime.now().date()
    tomorrow = today + timedelta(days=1)

    data = get_weather_data()
    # https://opendata.smhi.se/apidocs/metfcst/parameters.html

    for time_series in data["timeSeries"]:
        valid_time = datetime.fromisoformat(time_series["validTime"][:-1])
        date = valid_time.date()
        # print(json.dumps(time_series["parameters"], indent=4))
        if date == today:
            temp = next(
                param["values"][0]
                for param in time_series["parameters"]
                if param["name"] == "t"
            )
            wind = next(
                param["values"][0]
                for param in time_series["parameters"]
                if param["name"] == "ws"
            )
            weather = next(
                param["values"][0]
                for param in time_series["parameters"]
                if param["name"] == "Wsymb2"
            )
            if current_temp is None:
                current_weather = weather
                current_temp = temp
                current_wind = wind
            if today_high is None or temp > today_high:
                today_high = temp
                today_weather = weather
            if today_low is None or temp < today_low:
                today_low = temp

        if date == tomorrow:
            temp = next(
                param["values"][0]
                for param in time_series["parameters"]
                if param["name"] == "t"
            )
            if tomorrow_high is None or temp > tomorrow_high:
                tomorrow_high = temp
                tomorrow_weather = weather
            if tomorrow_low is None or temp < tomorrow_low:
                tomorrow_low = temp

    return (
        [current_weather, current_temp],
        current_wind,
        [today_weather, today_low, today_high],
        [tomorrow_weather, tomorrow_low, tomorrow_high],
    )


def bus_table(latest=DEFAULT_LATEST):
    url = "%s?accessId=%s&id=%s&format=json" % (
        RESROBOT_TIMETABLE_LEAVE_URL,
        TRAFIKLAB_KEY,
        TRAFIKLAB_SITEID,
    )

    arrive = leave = " n/a       "
    try:
        # get time now
        now = datetime.now().strftime("%H:%M:%S")
        arrive_time = latest["arrive"]
        leave_time = latest["leave"]
        dest = latest["destination"]

        print(now, arrive_time, leave_time)
        if now > arrive_time or now > leave_time:
            print(f"{now}, send request")
            result = requests.get(url, verify=False)
            data = result.json()
            buses = data["Departure"]
            # print(buses)
            for bus in buses[::-1]:  # it may return several reverse to overwrite
                # print("ok;?", bus)
                if bus["directionFlag"] == "1":
                    arrive_time = bus["time"][-8:]
                    latest["arrive"] = arrive_time
                else:
                    dest = bus["direction"][0]  # two destinations
                    latest["destination"] = dest
                    leave_time = bus["time"][-8:]
                    latest["leave"] = leave_time
        arrive = "\x7EV " + arrive_time
        leave = dest + "\x7F " + leave_time
        print(latest)
    except Exception as e:
        print(e)
    print(arrive, leave)
    return (arrive, leave)


def get_cpu_temp():  # get CPU temperature and store it into file "/sys/class/thermal/thermal_zone0/temp"
    # python3.9, don't use with .. except
    try:
        tmp = open("/sys/class/thermal/thermal_zone0/temp")
        cpu = tmp.read()
        tmp.close()
    except Exception:
        # mostly debug
        cpu = 50000  # 50c
    return "{:.1f}".format(float(cpu) / 1000) + "\x00C"


def get_time_now():  # get system time
    # https://astral.readthedocs.io/en/latest/index.html
    moon_str = ".{:02d}".format(int(moon.phase() / 28.0 * 100))
    str = datetime.now().strftime("%a     %H:%M:%S")
    # Mon     16:37:12
    # Mon .33 16:37:12
    return str[:4] + moon_str + str[7:]


def get_sunrise_now():
    city = LocationInfo("Location", "Country", ZONEINFO, LANTITUDE, LONGTITUDE)
    s = sun(city.observer, date=date.today(), tzinfo=zoneinfo.ZoneInfo(ZONEINFO))
    # ^ 06:12 16:37:12
    str = "^ %s %s" % (
        s["sunrise"].strftime("%H:%M"),
        datetime.now().strftime("%H:%M:%S"),
    )
    return str


def get_sunset_now():
    city = LocationInfo("Location", "Country", ZONEINFO, LANTITUDE, LONGTITUDE)
    s = sun(city.observer, date=date.today(), tzinfo=zoneinfo.ZoneInfo(ZONEINFO))
    # v 15:12 16:37:12
    str = "v %s %s" % (
        s["sunset"].strftime("%H:%M"),
        datetime.now().strftime("%H:%M:%S"),
    )
    return str


def loop():
    if not DEBUG:
        mcp.output(3, 1)  # turn on LCD backlight
        lcd.begin(16, 2)  # set number of LCD lines and columns
    arrive, leave = bus_table()
    seconds = 0
    mins = 0
    pos = 0

    (
        curr_wea,
        wind,
        today_wea,
        tomorr_wea,
    ) = get_weather()
    while True:
        if seconds == 0:
            arrive, leave = bus_table()
            seconds += 1
        elif seconds >= 60:
            seconds = 0
            if mins == 0:
                (
                    curr_wea,
                    wind,
                    today_wea,
                    tomorr_wea,
                ) = get_weather()
            elif mins >= 20:
                mins = 0
            else:
                mins += 1
        else:
            seconds += 1

        pos += 1
        if pos in range(1, 4):
            msg1 = "NOW:  Temp:%3.f\x00C" % curr_wea[1]
            msg2 = "%-5s Wind:%2.fm/s" % (get_weather_type(curr_wea[0]), wind)
        elif pos in range(4, 7):
            msg1 = "TDY:   Min:%3.f\x00C" % today_wea[1]
            msg2 = "%-5s  Max:%3.f\x00C" % (
                get_weather_type(today_wea[0]),
                today_wea[2],
            )
        elif pos in range(7, 10):
            msg1 = "TMRW:  Min:%3.f\x00C" % tomorr_wea[1]
            msg2 = "%-5s  Max:%3.f\x00C" % (
                get_weather_type(tomorr_wea[0]),
                tomorr_wea[2],
            )
        elif pos in range(10, 13):
            msg1 = "525  %s" % arrive
            msg2 = get_sunrise_now()
        elif pos in range(13, 16):
            msg1 = "525  %s" % leave
            msg2 = get_sunset_now()
        elif pos in range(16, 17):
            msg1 = "CPU Temp: " + get_cpu_temp()  # display CPU temperature
            msg2 = get_time_now()  # display the time
        elif pos > 17:
            pos = 0

        if not DEBUG:
            lcd.setCursor(0, 0)  # set cursor position

            lcd.message(msg1 + (16 - len(msg1)) * " " + "\n")
            lcd.message(msg2 + (16 - len(msg2)) * " ")
        print(seconds, len(msg1), msg1)
        print(seconds, len(msg2), msg2)
        sleep(1)


def destroy():
    if not DEBUG:
        lcd.clear()


def initial():
    global DEBUG, mcp, lcd
    print("initialize")
    from PCF8574 import PCF8574_GPIO
    from Adafruit_LCD1602 import Adafruit_CharLCD

    PCF8574_address = 0x27  # I2C address of the PCF8574 chip.
    PCF8574A_address = 0x3F  # I2C address of the PCF8574A chip.
    # Create PCF8574 GPIO adapter.
    try:
        mcp = PCF8574_GPIO(PCF8574_address)
    except:
        try:
            mcp = PCF8574_GPIO(PCF8574A_address)
        except:
            print("I2C Address Error !")
            DEBUG = True
            print("turn into debug mode for data testing!!")
    # Create LCD, passing in MCP GPIO adapter.
    lcd = Adafruit_CharLCD(pin_rs=0, pin_e=2, pins_db=[4, 5, 6, 7], GPIO=mcp)

    degreemark = bytes([0x0, 0xE, 0xA, 0xE, 0x0, 0x0, 0x0, 0x0])
    lcd.create_char(0, degreemark)
    lcd.clear()


if __name__ == "__main__":
    print("Program is starting ... ")
    if len(sys.argv) >= 2 and sys.argv[1] == "debug":
        DEBUG = True
    else:
        initial()
        DEBUG = False
    try:
        loop()
    except KeyboardInterrupt:
        destroy()

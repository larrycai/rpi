#!/usr/bin/env python
# coding: utf-8
import requests
import json
import os
from dotenv import load_dotenv
import urllib3
from time import sleep
from datetime import datetime

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# updated in 2024.10.5
# https://www.trafiklab.se/api/trafiklab-apis/resrobot-v21/
# old API
# https://developer.trafiklab.se/api/sl-realtidsinformation-4

load_dotenv()

TRAFIKLAB_KEY = os.getenv("TRAFIKLAB_KEY")
TRAFIKLAB_SITEID = os.getenv("TRAFIKLAB_SITEID")
RESROBOT_TIMETABLE_LEAVE_URL = "https://api.resrobot.se/v2.1/departureBoard"
DEFAULT_LATEST = {"arrive": "00:00:00", "leave": "00:00:00", "destination": "N"}


def bus_table(latest=DEFAULT_LATEST):
    url = "%s?accessId=%s&id=%s&format=json" % (
        RESROBOT_TIMETABLE_LEAVE_URL,
        TRAFIKLAB_KEY,
        TRAFIKLAB_SITEID,
    )

    arrive = leave = " n/a       "
    try:
        # get time now
        now = datetime.now().strftime("%H:%M:%S")
        arrive_time = latest["arrive"]
        leave_time = latest["leave"]
        dest = latest["destination"]

        print(now, arrive_time, leave_time)
        if now > arrive_time or now > leave_time:
            print(f"{now}, send request")
            result = requests.get(url, verify=False)
            data = result.json()
            buses = data["Departure"]
            # print(buses)
            for bus in buses[::-1]:  # it may return several reverse to overwrite
                # print("ok;?", bus)
                if bus["directionFlag"] == "1":
                    arrive_time = bus["time"][-8:]
                    latest["arrive"] = arrive_time
                else:
                    dest = bus["direction"][0]  # two destinations
                    latest["destination"] = dest
                    leave_time = bus["time"][-8:]
                    latest["leave"] = leave_time
        arrive = "\x7EV " + arrive_time
        leave = dest + "\x7F " + leave_time
        print(latest)
    except Exception as e:
        print(e)
    print(arrive, leave)
    return (arrive, leave)


def loop():
    arrive = leave = "n/a"
    seconds = 0
    odd = 0
    while True:
        odd = 1 - odd
        # print(seconds)
        if seconds == 0:
            arrive, leave = bus_table()
            seconds += 1
        elif seconds >= 60:
            seconds = 0
        else:
            seconds += 1
        if odd:
            msg1 = "BUSS   %s" % arrive
        else:
            msg1 = "MOT   %s" % leave
        print(msg1)
        sleep(1)


if __name__ == "__main__":
    print("Program is starting ... ")
    loop()

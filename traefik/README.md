# Introduction #

This is simple demo how to show the Live BUS information in LCD 16*2

![](pistation.png)

# Data

SL's data can be fetched from https://www.trafiklab.se/, need to register to get live bus data. 

https://developer.trafiklab.se/ login to get different token, lots of tokens are expired.

## GTFS Regional Realtime (not used): 

````
Brons, 0 / 30 000 allowed requests in the last 30d.
Key: 33298b17451a4406addb2b62c4c9daa1
````

## ResRobot v2.1

````
Bronze, 0 / 25 000 allowed requests in the last 30d.
Key: e9cd9fb9-xxxx
````

We need api under [ResRobot v2.1](https://www.trafiklab.se/api/trafiklab-apis/resrobot-v21/)

* Nearby stops for bus stop id according to Lat/Long (from google map)
* Timetables for departure/arrivals

* xxx bus: https://api.resrobot.se/v2.1/location.nearbystops?originCoordLat=xxx&originCoordLong=xxx&format=json&accessId=e9cd9fb9-xxx) - get bus stop id `xxx`
* xxx departure : https://api.resrobot.se/v2.1/departureBoard?id=xxx&format=json&accessId=e9cd9fb9-xxx)
* xxx arrivals : https://api.resrobot.se/v2.1/arrivalBoard?id=xxx&format=json&accessId=e9cd9fb9-xxx)

Interesting, `departureboard` & `arrivalboard` both have depart/arrival information, `departureboard` is backward compabitily with old data, has `directionFlag`, so it is used directly.

# Old data

token can be found under own profile

## siteId

from https://www.trafiklab.se/api/trafiklab-apis/sl/stops-and-lines-2/ to
 get `siteId` => `5036`

<pre>
curl "https://api.sl.se/api2/typeahead.json?key=b3xxx&searchstring=Tingvallaskolan" | jq
{
	"StatusCode": 0,
	"Message": null,
	"ExecutionTime": 0,
	"ResponseData": [
	{
		"Name": "Tingvallaskolan (Sigtuna)",
		"SiteId": "5036",
		"Type": "Station",
		"X": "17829780",
		"Y": "59624865",
		"Products": null
		},
	...
    }
}
</pre>

## livedata

from https://developer.trafiklab.se/api/sl-realtidsinformation-4 to get live data

<pre>
curl "https://api.sl.se/api2/realtimedeparturesv4.json?key=b1xxx&siteid=5036" | jq
{
	"StatusCode": 0,
	"Message": null,
	"ExecutionTime": 269,
	"ResponseData": {
		"LatestUpdate": "2021-11-21T19:08:46",
		"DataAge": 31,
		"Metros": [],
		"Buses": [
			{
				"GroupOfLine": null,
				"TransportMode": "BUS",
				"LineNumber": "575",
				"Destination": "Märsta station",
				"JourneyDirection": 2,
				"StopAreaName": "Tingvallaskolan",
				"StopAreaNumber": 52078,
				"StopPointNumber": 52078,
				"StopPointDesignation": null,
				"TimeTabledDateTime": "2021-11-21T19:11:42",
				"ExpectedDateTime": "2021-11-21T19:12:41",
				"DisplayTime": "3 min",
				"JourneyNumber": 21847,
				"Deviations": null
			},
			{
				"GroupOfLine": null,
				"TransportMode": "BUS",
				"LineNumber": "575",
				"Destination": "Ekilla gård",
				"JourneyDirection": 1,
				"StopAreaName": "Tingvallaskolan",
				"StopAreaNumber": 52078,
				"StopPointNumber": 52081,
				"StopPointDesignation": null,
				"TimeTabledDateTime": "2021-11-21T19:22:27",
				"ExpectedDateTime": "2021-11-21T19:22:27",
				"DisplayTime": "13 min",
				"JourneyNumber": 11915,
				"Deviations": null
			}
		],
...
</pre>

# show in LCD

## material

* Raspberry Pi Zero W
* I2C LCD 1602 from Freenove Super Starter Kit for Raspberry Pi, see https://www.amazon.se/Freenove-Raspberry-Tutorials-Solderless-Breadboard/dp/B06WP7169Y

Check doucmentation from [Freenove Super Starter Kit for Raspberry Pi](https://github.com/Freenove/Freenove_Super_Starter_Kit_for_Raspberry_Pi)

## Code 

now it is easily to shown in LCD. see code [I2CLCD1602](https://github.com/Freenove/Freenove_Super_Starter_Kit_for_Raspberry_Pi/blob/master/Code/Python_GPIOZero_Code/15.1.1_I2CLCD1602/I2CLCD1602.py)

* free token has limits, so better to check every minute
* LCD 16*2 switch arrive/leave every second

## run it from startup

put it under `/etc/rc.local` 

 